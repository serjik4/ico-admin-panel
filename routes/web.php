<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');

Route::get('register/activate/resend', 'AuthController@resendActivationCode');

Route::get('/2fa', 'Admin\Google2FAController@getValidateToken')->name('2fa');
Route::post('/2fa', 'Admin\Google2FAController@validateSecretKey')->name('2fa.validate');


Route::group(['middleware' => 'guest'], function() {
    Route::get('/register', 'AuthController@registerForm')->name('register');
    Route::post('/register', 'AuthController@register');
    Route::get('/login', 'AuthController@loginForm');
    Route::post('/login', 'AuthController@login')->name('login')->middleware('g2fa');
    Route::get('/register/activate', 'AuthController@activate')->name('activate');
});


Route::group(['middleware' => 'auth'], function() {
    Route::get('/logout', 'AuthController@logout')->name('logout');
});


Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function (){
    Route::get('/', 'DashboardController@index')->name('admin.index');
    Route::resource('/info', 'InfoController')->only(['index', 'store']);
    Route::get('/referral', 'ReferralController@index')->name('referral');
    Route::get('/settings', 'SettingController@index')->name('settings.index');
    Route::post('/settings', 'SettingController@store')->name('settings.store');
    Route::get('/2fa/enable', 'Google2FAController@enableTwoFactor')->name('g2fa.enable');
    Route::get('/2fa/disable', 'Google2FAController@disableTwoFactor')->name('g2fa.disable');
});

