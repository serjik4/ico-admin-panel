<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;


class Referrals extends Model
{
    protected $fillable = ['referral_link', 'user_id', 'parent_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /*
     * function to get first level referrals
     * */
    public static function getFirstLevelReferrals()
    {
        return DB::table('referrals as t1')
            ->select('t2.user_id as level1')
            ->leftJoin('referrals as t2', function($join) {
                $join->on('t2.parent_id', '=', 't1.user_id');
            })
            ->where('t1.user_id', '=', Auth::id())->whereNotNull('t2.user_id')->get()->all();

    }

    public static function getSecondLevelReferrals()
    {
        return DB::table('referrals as t1')
            ->select('t3.user_id as level2')
            ->leftJoin('referrals as t2', function($join) {
                $join->on('t2.parent_id', '=', 't1.user_id');
            })
            ->leftJoin('referrals as t3', function($join) {
                $join->on('t3.parent_id', '=', 't2.user_id');
            })
            ->where('t1.user_id', '=', Auth::id())->whereNotNull('t3.user_id')->get()->all();

    }

    public static function getThirdLevelReferrals()
    {
        return DB::table('referrals as t1')
            ->select('t4.user_id as level3')
            ->leftJoin('referrals as t2', function($join) {
                $join->on('t2.parent_id', '=', 't1.user_id');
            })
            ->leftJoin('referrals as t3', function($join) {
                $join->on('t3.parent_id', '=', 't2.user_id');
            })
            ->leftJoin('referrals as t4', function($join) {
                $join->on('t4.parent_id', '=', 't3.user_id');
            })
            ->where('t1.user_id', '=', Auth::id())->whereNotNull('t4.user_id')->get()->all();

    }

    public static function getFourthLevelReferrals()
    {
        return DB::table('referrals as t1')
            ->select('t5.user_id as level4')
            ->leftJoin('referrals as t2', function($join) {
                $join->on('t2.parent_id', '=', 't1.user_id');
            })
            ->leftJoin('referrals as t3', function($join) {
                $join->on('t3.parent_id', '=', 't2.user_id');
            })
            ->leftJoin('referrals as t4', function($join) {
                $join->on('t4.parent_id', '=', 't3.user_id');
            })
            ->leftJoin('referrals as t5', function($join) {
                $join->on('t5.parent_id', '=', 't4.user_id');
            })
            ->where('t1.user_id', '=', Auth::id())->whereNotNull('t5.user_id')->get()->all();

    }

    public static function countSubReferrals()
    {
        switch (Settings::getSetting('referral_level')) {
            case 1: return 0;
            case 2: return count(self::getSecondLevelReferrals());
            case 3: return count(self::getSecondLevelReferrals()) + count(self::getThirdLevelReferrals());
            case 4: return count(self::getSecondLevelReferrals()) + count(self::getThirdLevelReferrals()) + count(self::getFourthLevelReferrals());
            default: return 0;
        }
    }
}
