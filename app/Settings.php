<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable = ['key', 'value'];

    public static function getSetting($key)
    {
        return self::select('value')->where('key', $key)->value('value');
    }

    public static function setSetting($data)
    {
        $setting = new Settings();
        $setting->fill($data);
        $setting->save();

        return $setting;
    }

    public function updateSetting($data)
    {
        $this->fill($data);
        $this->save();
    }
}
