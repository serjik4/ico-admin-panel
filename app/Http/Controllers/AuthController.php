<?php

namespace App\Http\Controllers;

use App\Referrals;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class AuthController extends Controller
{

//    public function __construct()
//    {
//        $this->middleware(['2fa']);
//    }

    public function registerForm()
    {
        return view('register');
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|unique:users|email|min:5|max:35',
            'password' => 'required'
        ]);

        $user = User::add($request->all());
        $user->generatePassword($request->get('password'));
        $user->createUserFolder($request->get('email'));
        $user->activationCode = $this->generateActivationCode();
        $user->isActive = false;
        $user->isAdmin = false;
        $user->save();

        Log::info("User [{$user->email}] registered. Activation code: {$user->activationCode}");

        $this->sendActivationMail($user->id);

        /*
         * if isset referral cookie
         * add $parent_id to $data_referral
         * */
        if(Cookie::get('referral'))
        {
            $referral_link = Referrals::where('referral_link', Cookie::get('referral'))->first();

            if($referral_link)
            {
                $parent_id = $referral_link->user()->first()->id;//Call to a member function user() on null
            }
        }

        $data_referral = [
            'referral_link' => $user->id . str_random(8),
            'user_id' => $user->id,
            'parent_id' => isset($parent_id) ? $parent_id : null,
        ];

        Referrals::create($data_referral);

        return redirect('/login')->with('status', 'Thanks! Activation letter has been send to your email');
    }

    public function loginForm()
    {
        return view('login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if(Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ]))
        {
            /*
             * if user not active
             * propose to get a new activation link
             * */
            if(!Auth::user()->isActive)
            {
                $url = action('AuthController@resendActivationCode', ['q' => Auth::ID()]);//send user_id to resendActivationCode method
                $msg = 'You should activate your account. <a href="' . $url . '">Send a new activation link</a>';
                Auth::logout();
                return redirect()->back()->with('status', $msg);
            }
            return redirect('/admin/info')->with('status', 'Вы успешно вошли в систему');
        }

        return redirect()->back()->with('status', 'Неверный логин или пароль');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    protected function generateActivationCode()
    {
        return str_random('32');
    }

    protected function sendActivationMail($id)
    {
        $user = User::find($id);

        if(!$user)
        {
            return redirect()->route('index');
        }

        if(!$user->activationCode)
        {
            $user->activationCode = $this->generateActivationCode();
        }
        /*
         * generate activation link
         * */
        $activationUrl = action('AuthController@activate', [
            'id' => $user->id,
            'activationCode' => $user->activationCode,
        ]);

        Mail::send('emails.activationcode', ['activationUrl' => $activationUrl], function ($m) use ($user) {
            $m->to($user->email)->subject('Confirm your email');
        });
    }

    public function activate(Request $request)
    {
        /*
         * activate user
         * */
        $user = User::find($request->query('id'));

        if(!$user)
        {
            return redirect()->route('index')->with('status', 'Wrong activation link');
        }

        if($user->isActive && $user->activationCode != $request->query('activationCode'))
        {
            return redirect()->route('index')->with('status', 'Account has been already activated');
        }

        $user->activationCode = '';
        $user->isActive = true;
        $user->save();

        Log::info("User [{$user->email}] successfully activated");

        return redirect()->route('index')->with('status', 'Account has been activated');
    }

    public function resendActivationCode(Request $request)
    {
        $this->sendActivationMail($request->query('q'));
        return redirect()->back()->with('status', 'New activation code has been send');
    }
}
