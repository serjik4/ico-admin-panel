<?php

namespace App\Http\Controllers\Admin;

use Google2FA;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class Google2FAController extends Controller
{
    public function enableTwoFactor()
    {
        $user = Auth::user();
        if($user->google2fa_secret == null)
        {
            $secret = Google2FA::generateSecretKey(16);

            $user->google2fa_secret = $secret;
            $user->save();

            //generate image for QR barcode
            $imageUri = Google2FA::getQRCodeInline(
                config('app.name'),
                $user->email,
                $secret
            );

            return view('admin.g2fa.enable', [
                'image' => $imageUri,
                'secret' => $secret,
            ]);
        }
        return redirect()->route('admin.index')->with('status', '2fa has already enabled');
    }

    public function disableTwoFactor()
    {
        $user = Auth::user();
        if($user->google2fa_secret != null)
        {
            $user->google2fa_secret = null;
            $user->save();
            return view('admin.g2fa.disable');
        }
        return redirect()->route('admin.index');
    }

    public function getValidateToken()
    {
        if(session('2fa:user:id'))
        {
            return view('admin.g2fa.index');
        }
        return redirect('/login');
    }

    public function validateSecretKey(Request $request)
    {
        $this->validate($request, [
            'otp' => 'required'
        ]);

        if($this->validateToken($request))
        {
            if(Auth::loginUsingId($request->session()->get('2fa:user:id')))
            {
                return redirect('/admin/info')->with('status', 'Вы успешно вошли в систему');
            }
        }
        return redirect()->back()->with('status', 'Повторите попытку');
    }

    private function validateToken(Request $request)
    {
        $otp = $request->get('otp');

        $user = User::where('id', $request->session()->get('2fa:user:id'))->first();
        $valid = Google2FA::verifyKey($user->google2fa_secret, $otp);

        if($valid)
        {
            return true;
        }

        return false;
    }

//    public function random_string($length = 10) {
//        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
//        $charactersLength = strlen($characters);
//        $randomString = '';
//        for ($i = 0; $i < $length; $i++) {
//            $randomString .= $characters[rand(0, $charactersLength - 1)];
//        }
//        return $randomString;
//    }
}
