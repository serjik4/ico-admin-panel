<?php

namespace App\Http\Controllers\Admin;

use App\Referrals;
use App\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class ReferralController extends Controller
{
    public function index()
    {
        //dd(Config::get('app.referral_level'));
        if(Auth::check())
        {
            $user = Auth::user();
            return view('admin.referral', [
                'user' => $user,
                'referrals' => (!count(Referrals::getFirstLevelReferrals()) || !Settings::getSetting('referral_level')) ? 0 : count(Referrals::getFirstLevelReferrals()),
                'sub_referrals' => Referrals::countSubReferrals(),
            ]);
        }
        abort(404);
    }
}
