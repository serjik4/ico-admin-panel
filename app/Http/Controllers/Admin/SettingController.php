<?php

namespace App\Http\Controllers\Admin;

use App\Settings;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function index()
    {
        $referral_level = Settings::find('referral_level');

        return view('admin.setting', [
            'referral_level' => $referral_level,
        ]);
    }

    public function store(Request $request)
    {
        //dd($request->except('_token'));
        foreach ($request->except('_token') as $key => $value)
        {
            $setting = Settings::where('key', '=', $key)->get()->first();
//            dd($setting);
            if ($setting)
            {
                $setting->updateSetting([
                    'key' => $key,
                    'value' => $value,
                ]);
            } else {
                Settings::setSetting([
                    'key' => $key,
                    'value' => $value,
                ]);
            }
        }

        return redirect()->route('settings.index')->with('status', 'Настройки успешно сохранены');
    }
}
