<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;

class CheckReferralsMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        dd($request->query('ref'));
        if($request->query('ref'))
        {
            Cookie::queue('referral', $request->query('ref'), '4320');//4320 min == 3 day
            return $next($request);
            /*
             * return redirect('home');
             * */
        }

        return $next($request);
    }
}
