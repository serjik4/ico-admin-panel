<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Validator;

class Google2FAMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Validator::make($request->all(), [
            'email' => 'required|unique:users|email',
            'password' => 'required'
        ]);

        if(Auth::attempt([
            'email' => $request->get('email'),
            'password' => $request->get('password'),
        ]))
        {
            $user = Auth::user();

            if ($user && $user->google2fa_secret != null)
            {
                Auth::logout();

                $request->session()->put('2fa:user:id', $user->id);

                return redirect('/2fa');
            }
        }


        return $next($request);
    }
}
