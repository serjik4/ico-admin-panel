<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ICO</title>
</head>
<body>
<nav>
    <ul style="display: flex;">
        @if(Auth::check())
            <li style="margin-left: 20px;"><a href="{{route('index')}}">Home</a></li>
            <li style="margin-left: 20px;"><a href="{{route('admin.index')}}">Admin Panel</a></li>
            <li style="margin-left: 20px;"><a href="/logout">Logout</a></li>
        @else
            <li style="margin-left: 20px;"><a href="{{route('index')}}">Home</a></li>
            <li style="margin-left: 20px;"><a href="/register">Register</a></li>
            <li style="margin-left: 20px;"><a href="/login">Login</a></li>
        @endif
    </ul>
</nav>
    @if(session('status'))
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <div class="alert alert-danger">
                        <ul>
                            <li>{!! session('status') !!}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @include('errors')
    @yield('content')
</body>
</html>