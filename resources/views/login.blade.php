@extends('layout')

@section('content')
    <h1 style=>Sign in</h1>
    {{Form::open(['method' => 'post', 'route' => 'login'])}}
        <input type="text" name="email" value="{{old('email')}}" placeholder="Email">
        <input type="password" name="password" value="" placeholder="Password">
        <button type="submit">Login</button>
    {{Form::close()}}
@endsection