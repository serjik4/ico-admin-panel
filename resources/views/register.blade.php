@extends('layout')

@section('content')
    <h1 style=>Registration</h1>
    <form action="/register" method="post">
        {{csrf_field()}}
        <input name="email" type="text" value="{{old('email')}}" placeholder="Email">
        <input name="password" type="password" placeholder="Password">
        <button type="submit">Register</button>
    </form>
@endsection