@extends('layout')

@section('content')
    {{Form::open(['method' => 'post', 'route' => '2fa.validate'])}}
        <input type="text" name="otp">
        <button type="submit">Validate</button>
    {{Form::close()}}
@endsection