@extends('admin.layout')

@section('content')
    <h1>Google 2Factor Authentication</h1>
    <div class="g2fa-block">
        <img src="{{$image}}" alt="pic">
        <div class="secret-block">
            <h3 style="color: red;">Secret key:</h3>
            <p>{{$secret}}</p>
        </div>
    </div>
    <a href="/admin">Go Home</a>
@endsection