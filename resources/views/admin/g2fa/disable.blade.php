@extends('admin.layout')

@section('content')
    <h1>Google2FA Authentication has been disabled</h1>
    <a href="/admin">Go Home</a>
@endsection