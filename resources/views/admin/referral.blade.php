@extends('admin.layout')

@section('content')
    <h1>Referral page</h1>
    <div>
        <div class="info-ref-link" style="width: 30%;">
            <p style="margin: 0;">Referral link:</p>
            <input style="width: 100%;" type="text" value="{{route('register') .'?ref='. $user->referrals->referral_link}}" readonly>
        </div>
        <div class="info-ref-count" style="display: flex;flex-direction: column;">
            <p style="margin: 30px 0;">Count of referral users:<span> {{$referrals}}</span></p>
            <p style="margin: 0;">Count of sub-referral users:<span> {{$sub_referrals}}</span></p>
        </div>
    </div>
@endsection