<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ICO</title>
    <style type="text/css">

        .info-block {
            display: flex;
        }

        .info-form {
            display: flex;
            flex-direction: column;
            align-items: flex-end;
        }

        .info-table {
            margin-right: 20px;
        }

        .info-form-block {
            margin-right: 20px;
        }

        .info-form-block input{
            margin-bottom: 10px;
        }

        .g2fa-block {
            display: flex;
        }

        .setting-item {
            display: flex;
            align-items: center;
        }

        .setting-item p{
            margin-right: 15px;
        }
    </style>
</head>
<body>
<nav>
    <ul style="display: flex;">
        <li style="margin-left: 20px;"><a href="{{route('admin.index')}}">Home</a></li>
        <li style="margin-left: 20px;"><a href="{{route('info.index')}}">Info</a></li>
        <li style="margin-left: 20px;"><a href="{{route('referral')}}">Referral System</a></li>
        @if(Auth::user()->google2fa_secret)
            <li style="margin-left: 20px;"><a href="{{route('g2fa.disable')}}">Disable Google2FA</a></li>
        @else
            <li style="margin-left: 20px;"><a href="{{route('g2fa.enable')}}" onclick="return confirm('are you sure?')">Enable Google2FA</a></li>
        @endif
        <li style="margin-left: 20px;"><a href="{{route('settings.index')}}">Settings</a></li>
        <li style="margin-left: 20px;"><a href="{{route('logout')}}">Logout</a></li>
    </ul>
</nav>
    @if(session('status'))
        <div class="container">
            <div class="row">
                <div class="col-md-10">
                    <div class="alert alert-danger">
                        <ul>
                            <li>{!! session('status') !!}</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @yield('content')
</body>
</html>