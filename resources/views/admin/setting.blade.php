@extends('admin.layout')

@section('content')
    <h1>Admin Settings</h1>
    {{Form::open([
        'route' => 'settings.store',
        'method' => 'post',
    ])}}
    {{$referral_level}}
    <div class="settings-block">
        <div class="setting-item">
            <p>Referral level:</p>
            <select name="referral_level" id="referral_level">
                @for($i = 0; $i < 5; $i++)
                    @if(\App\Settings::getSetting('referral_level') == $i)
                        <option value="{{$i}}" selected>{{$i}}</option>
                    @else
                        <option value="{{$i}}">{{$i}}</option>
                    @endif
                @endfor
            </select>
        </div>
    </div>
    <button type="submit">Save settings</button>
    {{Form::close()}}
@endsection