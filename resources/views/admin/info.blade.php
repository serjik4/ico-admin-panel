@extends('admin.layout')

@section('content')
	<h1 style=>Personal information</h1>
	<div class="info-block">
		<div class="info-table">
			<table border="solid">
				<tr>
					<td>Email:</td>
					<td>@if($user->email != null){{$user->email}}@endif</td>
				</tr>
				<tr>
					<td>Name:</td>
					<td>@if($user->name != null){{$user->name}}@endif</td>
				</tr>
				<tr>
					<td>Last Name:</td>
					<td>@if($user->last_name != null){{$user->last_name}}@endif</td>
				</tr>
				<tr>
					<td>Phone:</td>
					<td>@if($user->phone_number != null){{$user->phone_number}}@endif</td>
				</tr>
				<tr>
					<td>BTC address:</td>
					<td>@if($user->btc != null){{$user->btc}}@endif</td>
				</tr>
			</table>
		</div>
		<div class="info-form-block">
			{{Form::open(['route' => 'info.store', 'class' => 'info-form'])}}
			<input type="text" name="name" placeholder="First Name" value="">
			<input type="text" name="last_name" placeholder="Last Name" value="">
			<input type="tel" name="phone_number" placeholder="Phone" value="">
			<input type="text" name="btc" placeholder="BTC address" value="">
			<button type="submit">Save information</button>
			{{Form::close()}}
		</div>
	</div>
@endsection